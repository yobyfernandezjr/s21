// console.log('Hello World');

// 1. The updated array after using a function that lets us add items into the array.
let wweLegends = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];
console.log(wweLegends);

function addWrestler(newWrestler){
	wweLegends[wweLegends.length] = newWrestler;
	// newWrestler = wweLegends[wweLegends.length + 1];
}

addWrestler("John Cena");
console.log(wweLegends);

// 2. The result of the variable which stores the array item returned by function that gets items in the users array by its index.

function getWrestlerIndex(index){
	return wweLegends[index];
};

let itemFound = getWrestlerIndex(2);
console.log(itemFound);

// 3. The result of the variable which stores the last item in the array returned by function that deletes the last item in the array and returns the last item in the array.

// console.log(wweLegends.length - 1);

function getLastItem(){
	let lastWrestler = wweLegends[wweLegends.length - 1];
	wweLegends.pop();
	return lastWrestler
};

lastWrestler = getLastItem();
console.log(lastWrestler);


// 4. The result of the variable which stores the last item in the array returned by function that deletes the last item in the array and returns the last item in the array.
// a. The updated users array after deletion.

// wweLegends.length--;
console.log(wweLegends);

// 5. The updated users array after using a function which allows us to update a specific item in the array by its index.

function updateWrestler(wrestler, index){
	wweLegends[index] = wrestler;
	// wrestler = wweLegends[index];
};

updateWrestler("Triple H", 3);
console.log(wweLegends);

function deleteWrestler(){
	wweLegends = [];
};

deleteWrestler();
console.log(wweLegends);

// console.log(wweLegends.length);

function checkArray(){
	if (wweLegends.length > 0){
		return false;
	} else {
		return true;
	};
};

let isUsersEmpty = checkArray();
console.log(isUsersEmpty);